# Development

## System Requirements

- NodeJS
- MySql

## Setup

Edit `./config/config.json` to your preferences. In the `development` section, change the username, password and name of the database. The `admin` section refers to the admin panel login credentials.

Install dependencies:

```
$ npm install
```
Populate the database:
```
$ ./node_modules/.bin/sequelize db:migrate
```
Build the frontend assets:
```
$ ./node_modules/.bin/gulp
```
Run local server
```
$ node index.js
```
Browse to http://127.0.0.1:3000

